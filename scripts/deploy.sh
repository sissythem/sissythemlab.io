#!/usr/bin/env bash
# default commit message
commit_message="Make site"
[ $# -eq 1 ] && [ "$1" == "help" ] && echo "Usage: $(basename $0) <commit message|e>"&& exit 1
if [ $# -gt 0 ]; then
	echo "gettings args, number : $#"
	# get the commit message strings
	commit_message="$@"
fi

echo "Building site..."
hugo --theme=blackburn

echo "Committing with message [$commit_message]..."
cd public
git add --all 
git commit -m "$commit_message"

echo "Uploading..."
git push origin master
